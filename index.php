<?php 
ini_set("display_errors", 1);
error_reporting(E_ALL);

//acessar template do admin: http://localhost/ecommerce-teste/index.php/admin
//acessar template do site: http://localhost/ecommerce-teste/index.php/
session_start();
require_once("vendor/autoload.php");

use \Slim\Slim;

$app = new Slim();

$app->config('debug', true);

require_once("site.php");
require_once("admin.php");
require_once("admin-users.php");
require_once("admin-categories.php");
require_once("admin-products.php");
require_once("admin-orders.php");
require_once("functions.php");

$app->run();
 ?>