<?php

use \Hcode\PageAdmin;
use \Hcode\Model\User;

$app->get('/admin', function() {
	User::verifyLogin();

	$page = new PageAdmin();
	$page->setTpl("index");
});

$app->get('/admin/login', function() {
	//desabilitar o header e o footer automaticos pois essa página é diferente das demais
	$page = new PageAdmin([
		"header"=>false,
		"footer"=>false
	]);
	$page->setTpl("login");
});

$app->post('/admin/login', function(){
	User::login($_POST["login"], $_POST["password"]);
	header("Location: /ecommerce-teste/index.php/admin");
	exit;
});

$app->get('/admin/logout', function(){
	User::logout();
	header("Location: /ecommerce-teste/index.php/admin/login");
	exit;
});

$app->get('/admin/forgot', function(){
	$page = new PageAdmin([
		"header"=>false,
		"footer"=>false
	]);
	$page->setTpl("forgot");
});

$app->post('/admin/forgot', function(){
	$user = User::getForgot($_POST["email"]);

	header("Location: /ecommerce-teste/index.php/admin/forgot/sent");
	exit;
});

$app->get('/admin/forgot/sent', function(){
	$page = new PageAdmin([
		"header"=>false,
		"footer"=>false
	]);
	$page->setTpl("forgot-sent");
});


?>