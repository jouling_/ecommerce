<?php

use \Hcode\PageAdmin;
use \Hcode\Model\User;
use \Hcode\Model\Order;
use \Hcode\Model\OrderStatus;

//deletar pedido
$app->get("/admin/orders/:idorder/delete", function($idorder){
    User::verifyLogin();

    $order = new Order();
    $order->get((int)$idorder);
    $order->delete();

    header("Location: /ecommerce-teste/index.php/admin/orders");
    exit;
});

//ver status do pedido
$app->get("/admin/orders/:idorder/status", function($idorder){
    User::verifyLogin();

    $order = new Order();
    $order->get((int)$idorder);

    $page = new PageAdmin();
    $page->setTpl("order-status", [
        'order'=>$order->getValues(),
        'status'=>OrderStatus::listAll(),
        'msgError'=>Order::getError(),
        'msgSuccess'=>Order::getSuccess()
    ]);
});

$app->post("/admin/orders/:idorder/status", function($idorder){
    User::verifyLogin();

    if(!isset($_POST['idstatus']) || !(int)$_POST['idstatus'] > 0){
        Order::setError("Informe um status.");

        header("Location: /ecommerce-teste/index.php/admin/orders/" . $idorder . "/status");
        exit;
    }
    
    $order = new Order();
    $order->get((int)$idorder);

    $order->setidstatus((int)$_POST['idstatus']);
    $order->save();

    Order::setSuccess("Status atualizado");
    header("Location: /ecommerce-teste/index.php/admin/orders/" . $idorder . "/status");
    exit;
});

//ver detalhes do pedido
$app->get("/admin/orders/:idorder", function($idorder){
    User::verifyLogin();

    $order = new Order();
    $order->get((int)$idorder);
    
    $cart = $order->getCart();

    $page = new PageAdmin();
    $page->setTpl("order", [
        'order'=>$order->getValues(),
        'cart'=>$cart->getValues(),
        'products'=>$cart->getProducts()
    ]);
});

//ver todos os pedidos
$app->get("/admin/orders", function(){
    User::verifyLogin(); 
 
	$search = (isset($_GET['search'])) ? $_GET['search'] : "";
	$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

	if($search != ''){
		$pagination = Order::getPageSearch($search, $page, 3);
	} else{
		$pagination = Order::getPage($page, 5);
	}

	$pages = [];

	for ($x=0; $x < $pagination['pages']; $x++) { 
		array_push($pages, [
			'href'=>'/ecommerce-teste/index.php/admin/orders?'.http_build_query([
				'page'=>$x+1,
				'search'=>$search
			]),
			'text'=>$x+1
		]);
	}

    $page = new PageAdmin();
    $page->setTpl("orders", [
        'orders'=>$pagination['data'],
		"search"=>$search,
		"pages"=>$pages
    ]);
 });

?>