<?php

use \Hcode\PageAdmin;
use \Hcode\Model\User;

$app->get('/admin/users/:iduser/password', function($iduser){
	User::verifyLogin();
	
	$user = new User();
	$user->get((int)$iduser);

	$page = new PageAdmin();
	$page->setTpl("users-password", [
		"user"=>$user->getValues(),
		"msgError"=>User::getError(),
		"msgSuccess"=>User::getSuccess()
	]);
});

$app->post("/admin/users/:iduser/password", function($iduser){
	User::verifyLogin();
	
	if(!isset($_POST['despassword']) || $_POST['despassword'] === ''){
		User::setError("Preencha a nova senha.");

		header("Location: /ecommerce-teste/index.php/admin/users/$iduser/password");
		exit;
	}

	if(!isset($_POST['despassword-confirm']) || $_POST['despassword-confirm'] === ''){
		User::setError("Confirme a nova senha.");

		header("Location: /ecommerce-teste/index.php/admin/users/$iduser/password");
		exit;
	}

	if($_POST['despassword'] !== $_POST['despassword-confirm']){
		User::setError("As senhas nao sao iguais.");

		header("Location: /ecommerce-teste/index.php/admin/users/$iduser/password");
		exit;
	}

	$user = new User();
	$user->get((int)$iduser);
	$user->setPassword(User::getPasswordHash($_POST['despassword']));

	User::setSuccess("Senha atualizada com sucesso.");

	header("Location: /ecommerce-teste/index.php/admin/users/$iduser/password");
	exit;
});

//tela que lista todos os usuários do sistema
$app->get('/admin/users', function(){
	User::verifyLogin();

	$search = (isset($_GET['search'])) ? $_GET['search'] : "";
	$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;

	if($search != ''){
		$pagination = User::getPageSearch($search, $page, 3);
	} else{
		$pagination = User::getPage($page, 2);
	}

	$pages = [];

	for ($x=0; $x < $pagination['pages']; $x++) { 
		array_push($pages, [
			'href'=>'/ecommerce-teste/index.php/admin/users?'.http_build_query([
				'page'=>$x+1,
				'search'=>$search
			]),
			'text'=>$x+1
		]);
	}

	/*print_r($pages);
	var_dump($pagination['data']);
	var_dump($search);
	exit;*/

	$page = new PageAdmin();
	$page->setTpl("users", array(
		"users"=>$pagination['data'],
		"search"=>$search,
		"pages"=>$pages
	));
});

//tela para criar usuários; responde com HTML
$app->get('/admin/users/create', function(){
	User::verifyLogin();

	$page = new PageAdmin();
	$page->setTpl("users-create");
});

//para apagar um usuário
$app->get('/admin/users/:iduser/delete', function($iduser){
	User::verifyLogin();	
	$user = new User();
	$user->get((int)$iduser);
	$user->delete();

	header("Location: /ecommerce-teste/index.php/admin/users");
	exit;
});

//tela para atualizar usuários
//o usuário passado como parâmetro na função já cairá direto na rota
$app->get('/admin/users/:iduser', function($iduser){
	User::verifyLogin();

	$users = new User();

	$users->get((int)$iduser);

    $page = new PageAdmin();

	$page->setTpl("users-update", array(
		"users" => $users->getValues()
	));

});

//tela de criação de usuarios; enviará os dados para o banco
$app->post('/admin/users/create', function(){
	User::verifyLogin();	
	$user = new User();

	$_POST["inadmin"] = (isset($_POST["inadmin"]))?1:0;

	$user->setData($_POST);
	$user->save();
	header("Location: /ecommerce-teste/index.php/admin/users");
	exit;
});

//tela de atualização dos dados; pra salvar a edição
$app->post('/admin/users/:iduser', function($iduser){
	User::verifyLogin();

	$user = new User();
	
	$_POST["inadmin"] = (isset($_POST["inadmin"]))?1:0;
	$user->get((int)$iduser);
	$user->setData($_POST); 
	$user->update();

	header("Location: /ecommerce-teste/index.php/admin/users");
	exit;
});

?>